stringx = require 'pl.stringx'
inspect = require 'inspect'

pre_process = () ->
	{stringx.rstrip(word), true for word in io.lines('./enable1.txt')}

permute = (word, real_words) ->
	p = {words: {}, n: 0}
	for i=1, #word
		new =  word\sub(1, i - 1) .. word\sub(i+1, #word)
		if real_words[new] and not p.words[new]
			p.words[new] = true
			p.n += 1
	return p

bonus2 = () ->
	word_map = pre_process()
	matchy_words = {}
	for word in pairs word_map
		p = permute(word, word_map)
		if p.n >= 5
			matchy_words[word] = p.words
	return matchy_words

matches = bonus2()
for k in pairs matches
	print k
