#=
This is a julia port of helloevolve.py

Original can be found here:
https://gist.github.com/josephmisiti/940cee03c97f031188ba7eac74d03a4f

helloevolve.jl implements a genetic algorithm that stats with a base
population of randomly generated strings, interates over a certain number of
generations while implementing 'natural selection', and prints out the most fit
string.

The parameters of the simulatin can be changed by modifying one fo the initial 
variables in main. To chane the "most fit" strin, modify OPTIMAL. POP_SIZE
controls the size of each generation, GENERATIONS is the amount of
generations that the simulation will loop through before returning the fittest string.
=#

# Return a random character between ASCII 32 and 126 (i.e. spaces, symbols,
# letters, and digits). All characters returned will be nicely pritable.
function random_char()
    return  string(Char(rand(32:126)))
end

# Return a list of pop_size individuals, each randomly generated via iterating
# dna_size times to generate a string of random characters with random_char().
function random_population(pop_size::Int64, dna_size::Int64)
    pop = String[]
    for i = 1:pop_size
        dna = ""
        for c = 1:dna_size
            dna *= random_char()
        end
        push!(pop, dna)
    end
    return pop
end

# For each gene in the DNA, this function calculates the difference between
# it and the character in the sam eposition in the optimal string. These values
# are summed and then returned.
function fitness(dna::String, optimal::String, dna_size::Int64)
    fitness_value = 0
    for c = 1:dna_size
        fitness_value += abs(Int(dna[c]) - Int(optimal[c]))
    end
    return convert(Float64, fitness_value)
end

# Chooses a random element from items, where items is a array of array in
# the form [item, weight]. Weight determins the probability of choosing its
# respective item. Note: this function is borrowed fro ActiveState Recipes.
function weighted_choice(items::Array{Array{Any}})
    weighted_total = sum([item[2] for item in items])
    rand_uniform(x::Float64, y::Float64) = x + (y - x) * rand() 
    n = rand_uniform(0.0, weighted_total)
    for (item, weight) in items
        if n < weight
            return item
        end
        n = n - weight
    end
    return items[end][1]
end

# Slices both dna1 and dna2 into two parts at a random index within their
# length and merges them. Both keep their initial sublist up to the crossover
# index, but their ends are swapped.
function crossover(ind1::String, ind2::String, dna_size::Int64)
    pos = floor(Int64, rand()*dna_size) + 1
    return (ind1[1:pos]*ind2[pos+1:end], ind2[1:pos]*ind1[pos+1:end])
end

# For each gene in the DNA, there is a 1/mutation_chance chance that it will be
# switched out with a random character. This ensures diversity in the population,
# and ensures that it is difficult to get stuck in local minima.
function mutate(dna::String, dna_size::Int64)
    dna_out = ""
    mutation_chance = 100
    for c = 1:dna_size
        if floor(Int64, rand()* mutation_chance) == 1.0
            dna_out *= random_char()
        else
            dna_out *= string(dna[c])
        end
    end
    return dna_out
end

function main()
    optimal = "Hello, World"
    dna_size = length(optimal)
    pop_size = 20
    generations = 5000

    # Generate initial population. This will create a list of pop_size strings,
    # each initialized to a sequence of random characters.
    population = random_population(pop_size, dna_size)
    start_example = population[1]

    # Simulate all of the generations
    for generation = 1:generations
        println("Generation $generation... Random sample: $(population[1])")
        weighted_population = Array{Array{Any}}(pop_size)

        # Add individuals and their respective fitness levels to the weighted
        # population list. This will be used to pull out individuals via certain
        # probabilities during the selection phase. Then, reset the population list
        # so we can repopulate it after selection.
        for (index, individual) in enumerate(population)
            fitness_val = fitness(individual, optimal, dna_size)
            if fitness_val == 0
                pair = [individual, 1.0]
            else
                pair = [individual, 1.0/fitness_val]
            end
            weighted_population[index] = pair
        end

        population = String[]

        # Select two random individuals, based on their fitness probabilities, cross
        # their genes over at a random point, mutate them, and add them back to the
        # populatin for the next iteration
        for _ = 1:(pop_size/2)
            # selection
            ind1 = weighted_choice(weighted_population)
            ind2 = weighted_choice(weighted_population)

            # crossover
            ind1, ind2 = crossover(ind1, ind2, dna_size)

            # mutate and add back into the population
            push!(population, mutate(ind1, dna_size))
            push!(population, mutate(ind2, dna_size))
        end

    end
    # Display the highest-ranked string after all generations have been iterated
    # over. This will be the closes string to the optimal string, meaning it
    # will have the smallest fitness value.
    fittest_string = population[1]
    minimum_fitness = fitness(population[1], optimal, dna_size)

    for individual in population
        ind_fitness = fitness(individual, optimal, dna_size)
        if ind_fitness <= minimum_fitness
            fittest_string = individual
            minimum_fitness = ind_fitness
        end
    end
    println("Fittest String: $fittest_string")
    println("Evolved $start_example to $fittest_string over $generations generations")
end

main()