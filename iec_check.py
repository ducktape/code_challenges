def i_before_e(word):
  return 'cie' not in word and word.count('ei') == word.count('cei')

with open('./enable1.txt') as in_fh:
	print(len([word for word in in_fh if not i_before_e(word)]))