#!/usr/bin/env julia
using Memoize
#=
This is a julia port of helloevolve.py

Original can be found here:
https://gist.github.com/josephmisiti/940cee03c97f031188ba7eac74d03a4f

helloevolve.jl implements a genetic algorithm that stats with a base
population of randomly generated strings, interates over a certain number of
generations while implementing 'natural selection', and prints out the most fit
string.

The parameters of the simulatin can be changed by modifying one fo the initial 
variables in main. To chane the "most fit" strin, modify OPTIMAL. POP_SIZE
controls the size of each generation, GENERATIONS is the amount of
generations that the simulation will loop through before returning the fittest string.
=#

# Return a random character between ASCII 32 and 126 (i.e. spaces, symbols,
# letters, and digits). All characters returned will be nicely pritable.
function random_char()
    return  string(Char(rand(97:122)))
end

# Return a list of pop_size individuals, each randomly generated via iterating
# dna_size times to generate a string of random characters with random_char().
function random_population(pop_size::Int64, dna_size::Int64)
    pop = String[]
    for i = 1:pop_size
        dna = ""
        for c = 1:dna_size
            dna *= random_char()
        end
        push!(pop, dna)
    end
    return pop
end

# For each gene in the DNA, this function calculates the number of words
# that can be found in the patter (dna), and takes into account the length
# of the pattern
function fitness(dna::String, words::Array{String})
    fitness_value = length(words) + 1
    for word in words
        if checker(word, dna)
            fitness_value -= 1
        end
    end
    convert(Float64, fitness_value)
    fitness_value = abs(fitness_value - (1/length(dna)))
    return fitness_value
end

# Chooses a random element from items, where items is a array of array in
# the form [item, weight]. Weight determins the probability of choosing its
# respective item. Note: this function is borrowed fro ActiveState Recipes.
function weighted_choice(items::Array{Array{Any}})
    weighted_total = sum([item[2] for item in items])
    rand_uniform(x::Float64, y::Float64) = x + (y - x) * rand() 
    n = rand_uniform(0.0, weighted_total)
    for (item, weight) in items
        if n < weight
            return item
        end
        n = n - weight
    end
    return items[end][1]
end

# Slices both dna1 and dna2 into two parts at a random index within their
# length and merges them. Both keep their initial sublist up to the crossover
# index, but their ends are swapped.
function crossover(ind1::String, ind2::String)
    dna_size = min(length(ind1), length(ind2))
    pos = floor(Int64, rand()*dna_size) + 1
    return (ind1[1:pos]*ind2[pos+1:end], ind2[1:pos]*ind1[pos+1:end])
end

# For each gene in the DNA, there is a 1/mutation_chance chance that it will be
# switched out with a random character. This ensures diversity in the population,
# and ensures that it is difficult to get stuck in local minima.
function mutate(dna::String)
    dna_size = length(dna)
    dna_out = ""
    mutation_chance = 100
    for c = 1:dna_size
        chance = floor(Int64, rand()* mutation_chance)
        # change a char
        if chance >= 98.0
            dna_out *= random_char()
        # delete a char
        elseif chance <= 2.0 && dna_size > 10
            dna_out = dna_out
        # add a char
        elseif chance > 2.0 && chance <= 4.0
            dna_out *= string(dna[c])
            dna_out *= random_char()
        else
            dna_out *= string(dna[c])
        end
    end
    return dna_out
end

function find_fittest(words::Array{String}, pop_seed::Array{String})
    dna_size = 50 # Size that we start with at least
    pop_size = 20
    generations = 1000

    pop_size = pop_size - length(pop_seed)
    # Generate initial population. This will create a list of pop_size strings,
    # each initialized to a sequence of random characters.
    population = random_population(pop_size, dna_size)
    [push!(population, seed) for seed in pop_seed]
    pop_size = length(population)
    start_example = population[1]
    solved = String[]

    # Simulate all of the generations
    for generation = 1:generations
        println("Generation $generation... Random sample: $(population[1])")
        weighted_population = Array{Array{Any}}(pop_size)

        # Add individuals and their respective fitness levels to the weighted
        # population list. This will be used to pull out individuals via certain
        # probabilities during the selection phase. Then, reset the population list
        # so we can repopulate it after selection.
        for (index, individual) in enumerate(population)
            fitness_val = fitness(individual, words)
            pair = [individual, fitness_val]
            # if fitness_val == 0
            #     pair = [individual, 1.0]
            # else
            #     pair = [individual, 1.0/fitness_val]
            # end
            weighted_population[index] = pair
        end

        [push!(solved, i[1]) for p in population if all([checker(w, p) for w in words])]

        population = String[]

        # Select two random individuals, based on their fitness probabilities, cross
        # their genes over at a random point, mutate them, and add them back to the
        # populatin for the next iteration
        for _ = 1:(pop_size/2)
            # selection
            ind1 = weighted_choice(weighted_population)
            ind2 = weighted_choice(weighted_population)

            # crossover
            ind1, ind2 = crossover(ind1, ind2)

            # mutate and add back into the population
            push!(population, mutate(ind1))
            push!(population, mutate(ind2))
        end

    end
    # Display the highest-ranked string after all generations have been iterated
    # over. This will be the closes string to the optimal string, meaning it
    # will have the smallest fitness value.
    fittest_string = population[1]
    minimum_fitness = fitness(population[1], words)

    for individual in population
        ind_fitness = fitness(individual, words)
        if ind_fitness <= minimum_fitness
            fittest_string = individual
            minimum_fitness = ind_fitness
        end
    end
    println("Fittest String: $fittest_string")
    println("Evolved $start_example to $fittest_string over $generations generations")
    return fittest_string, solved
end


@memoize function checker(word::String, possible::String)
    MAX = 256

    function compare(arr1::Array{Int64}, arr2::Array{Int64})
        for i in 1:MAX
            if arr1[i] != arr2[i]
                return false
            end
        end
        return true
    end


    function search(pattern::String, text::String)
        m = length(pattern)
        n = length(text)

        count_pattern = zeros(Int64, MAX)
        count_text_window = zeros(Int64, MAX)
        if n < m
            return false
        end

        for i in 1:m
            count_pattern[Int(pattern[i])] += 1
            count_text_window[Int(text[i])] += 1
        end

        for i in m+1:n
            if compare(count_pattern, count_text_window)
                return true
            end

            count_text_window[Int(text[i])] += 1
            count_text_window[Int(text[i-m])] -= 1
        end

        if compare(count_pattern, count_text_window)
            return true
        end
        return false
    end

    function slice(x::AbstractString, step::Int64)
        return join([x[i] for i in 1:step:length(x)], "")
    end

    function generate_slices(x::AbstractString)
        return (slice(x[i:j], k) for i in 1:length(x) for j in i:length(x) for k in 1:length(x[i:j]))
    end

    # Go through iterations of slices of possible and see if word is an anagram
    for possible_slice in generate_slices(possible)
        if search(word, possible_slice)
            return true
        end
    end
    return false
end


function main()
    # in_fh = open("./363-hard-words.txt")
    # input_words = [line for line in eachline(in_fh)]
    # close(in_fh)
    # seed_starting = ["ptesognesarlioaldseelttyarpmsisgnaisbewurwanaomtdyccoirskeoaekcharldlunnydubvtsegbpliahktisiffoburdeselkotcogrfrrosumgpwkmhoaesttnacpblgwalufymaicohwmarezpsyntlojnhuejmipdupeczafeveigabnvyhuypayedgsxsplovqzsjsfjyexsoqdujcegifkinkdbpodizlabijbnnoayoafznehpftdtfockwnirauumkmmmzbuz", "ptesognesarliotamsesabgbdipnsnawhraodteilkseodculasyoptlfilmabcroavrseolrtyetfuflmyenaipblkcmrsiehdeydgrknguasskocwwhgndawtrbsuesusuduoohswbchefinmojttrgdalkflpbjuzingiailmayzvhankeipomaayrkecgfmzabpemhohhkeccuojbjkuufjtmtpesdvyniozhzpdouqnrdlaxwxntetyesuisfyewvsjqyodgyiamxupgaffibjbwosoabwttuuai", "mfwfnxqeijltbrzepdetffujujlrofkhynnezwoulwuzoyirzczxeybtmlxbspazgdojnldomoahsicipnrfmobcdloocyeiskpwrfuklpttbeoohmuccwusbrhebtutudaidekyrtaelsdapinssoelapmstfdrakoreiesnagtgyweonpheilalcsmistsbajuynggorovgfaiharfnhiljlnuaiegiyxagmpazrqvatamsksoibaygmdjgnmhvaseuhenikkillwqimnxcw", "abeddyankafsabsaideabetabughsagoalacediesainbyarearainfoafsalpadsangaesarkailackakaphatadscabaleachaemaaramicachyaudadobiscoptskscudarkatscumboboeastaelksentailksexyaupalyechawaffactamemshindarnaaneumageedawteedgyvendeeditsimslipeephonesinsnugaffardeereekeirevsithewallawneonodskewarmazetamylibslumazyillsoftiffyobsoothiogremojockinkitsownoggapyoweurbspazapspundyuckisstuffarloboliordoffossbuttingigsrimytheckolouchiveluffoxyurtorrockophututubundoxybuzzingogomurkvatsbiffozyjismeowhenclopepsdullwillaquaimmymullpraymigszoicrawhinjeerqatsjabsjavadudejeuxpushoggulpuffquodozyjinnjibbizejujustquipmozoqophjaukvidecurnwirymumminxvugg", "abetheweraspssteyeragibsabsadiesafeatmaarbsagoalaneapeddyadossainbyteedadoffactabugsaltacedikeckafsarialbsarkainfontarpaidemoanilkscanaanabeyscudarneemicaulampscumbogyvendawtaxachyankaphateffeltiffyaupalyesexyechawolioboecuseshineumanyellawneventintirlickempasharlimpsildeereekidsiltofferevskegafflamazeeskewafflogamazyillskiphutoitorraquarvodylobombillochowslubizengsmugaunoggeedevsnuboobotadouroominxoopspazigsudsumsunnunsjotsjaukitswitewhenhulkpreplopocoryodsdrewarmottuckmummythazyuckjiltutujohndoxycurbuttprezinglegoshoggwigsjigsjackjibbuzzqatsvuggcozyjurywhizjugswarypurlcrocwhomfozyqophushwildulljinnpunkpuffjismjujujimpowed", "abaledicandrsupartabeayslhlniepiofsntofmherdiesgoaiolobaetglsekotpwkfshddiarorcsbamkpuyslwrnyaalmytenkauenitgggeaepnsmhcukpdsasresbydcgvcvouemfejcriaivgapflvmljgtretyhwlhpharitzuoezneawbaobjehfctrplowrijhbcepduwsseosbbushmoaiyggqoiupafhukubxcocbadofwfnoltzilfxkmedglablnfymuumabgiaajfaoninnyaavzmaaabaodevsjujukinkmummtutu"]
    # possible = "oufrirvaewstnoeaxh"
    seed_starting = ["oufrirvaewstnoeaxh"]
    input_words = ["one", "two", "three", "four", "five", "six", "seven", "ten"]
    fit, solved = find_fittest(input_words, seed_starting)
    check = all([checker(word, fit) for word in input_words])
    println("$(length(fit)):$check -> $fit")
    for s in solved
        check = all([checker(word, s) for word in input_words])
        println("$(length(s)):$check -> $s")
    end

end

main()