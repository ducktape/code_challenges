    using DataStructures

    function strip_nums(str::AbstractString)
        replace(str, r"\d", "")
    end

    function eval_pattern(str::AbstractString)
        matches = eachmatch(r"\d", str)
        patterns= Tuple{Int64,Int64}[]
        offset = 0
        for x in matches
            offset += 1
            push!(patterns, (parse(x.match), x.offset - offset))
        end
        return patterns
    end

    function seen_prefix(t::Trie, str)
        seen = []
        for x in path(t, str)
            if x.is_key
                push!(seen, x)
            end
        end
        return seen
    end

    # Read in the patterns
    # They will be in a dict of the form {prefix: [(value, index_in_prefix)..]}
    input_words = [".mistranslate.", ".alphabetical.", ".bewildering.", ".buttons.", ".ceremony.", ".hovercraft.", ".lexicographically.", ".programmer.", ".recursion."]
    input_file = "./tex-hyphenation-patterns.txt"
    inputs = Dict{String,Array{Tuple{Int64,Int64}}}()
    open(input_file) do in_fh
        for line in eachline(in_fh)
            inputs[strip_nums(line)] = eval_pattern(line)
        end
    end

    # Initialize the Trie 
    trie = Trie(inputs)

    for word in input_words
        wordlen = length(word)
        word_vals = zeros(Int64, wordlen)
        # Walk down the word and get all the prefix's for that suffix
        for i in 1:wordlen
            seen = seen_prefix(trie, word[0+i:wordlen])
            for suffix in seen
                # Insert the values from the suffix into the word_vals
                for v in suffix.value
                    if v[1] > word_vals[i + v[2]]
                        word_vals[i + v[2]] = v[1]
                    end
                end
            end
        end

        # Now insert the hyphens at the odd values
        # skip the first and last char to remove the '.'s
        offset = 0
        hyph_word = word[2:end-1]
        word_vals = word_vals[2:end-1]
        for i in 1:length(word_vals)
            v = word_vals[i]
            if v != 0 && isodd(v)
                hyph_word = hyph_word[1:i+offset-1] * "-" * hyph_word[i+offset:end]
                offset += 1
            end
        end
        println("$hyph_word => $hyph_word")
    end