require 'luarocks.loader'
local inspect = require 'inspect'

--Check if w2 can be made from w1 by removing 1 letter
function funnel(big_word, small_word)
    local m, n = 1, 1
    local deletions = 0

    while deletions <= 1 and m <= #big_word do
        if big_word:sub(m, m) == small_word:sub(n, n) then
            -- advance both
            m = m + 1
            n = n + 1
        else
            -- advance big only
            deletions = deletions + 1
            m = m + 1
        end
    end
    return deletions <= 1
end

-- Get all variations of a single deletion for word and return as a set
function permute(word, real_words)
    local p = {words = {}, n = 0}
    -- delete whatever i is and add to set
    for i=1, #word do
        local pre = word:sub(1, i - 1)
        local post = word:sub(i+1)
        local del = pre .. post
        if real_words[del] and not p.words[del] then
            p.words[del] = true
            p.n = p.n + 1
        end
    end
    return p
end

function pre_process()
    local real_words = {}
    for word in io.lines('./enable1.txt') do
        word = trim(word)
        real_words[word] = true
    end
    return real_words
end

function trim(s)
   return s:match'^%s*(.*%S)' or ''
end

function bonus2()
    local real_words = pre_process()
    local matchy_words = {}
    for word in pairs(real_words) do
        local p = {words = {}, n = 0}
        -- delete whatever i is and add to set
        for i=1, #word do
            local pre = word:sub(1, i - 1)
            local post = word:sub(i+1)
            local del = pre .. post
            if real_words[del] and not p.words[del] then
                p.words[del] = true
                p.n = p.n + 1
            end
        end
        -- local p = permute(word, real_words)
        if p.n >= 5 then
            matchy_words[word] = p.words
        end
    end
    return matchy_words
end

--Find all the words in enable1 that are one deletion away
function bonus1(test_word)
    local real_words = pre_process()
    local p = permute(test_word, real_words)
    return p.words
end

-- Bonus 2 
matches = bonus2()
for k in pairs(matches) do print(k) end

-- -- Bonus 1
-- print(inspect(bonus1('dragoon')))
-- print(inspect(bonus1('boats')))
-- print(inspect(bonus1('affidavit')))

-- -- Challenge
-- print(funnel('leave', 'eave'))
-- print(funnel('reset', 'rest'))
-- print(funnel('dragoon', 'dragon'))
-- print(funnel('eave', 'leave'))
-- print(funnel('sleet', 'lets'))
-- print(funnel('skiff', 'ski'))


