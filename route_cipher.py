import numpy as np
import re
import sys

def get_next_letter(grid, cords, direction):
    c_u_limit= cords[1] - 1
    c_l_limit = 0
    r_u_limit = cords[0] - 1
    r_l_limit = 0

    start = [r_l_limit, c_u_limit]
    seen=[start]
    yield grid[start[0], start[1]]
    while len(seen) < cords[0] * cords[1]:
        c = [start[0] - 1, start[1]]
        if c[0] == r_l_limit and c[1] == c_u_limit:
            c[0] += 1
            r_l_limit += 1
            print("top right")
        elif c[0] == r_u_limit and c[1] == c_u_limit:
            c[1] -= 1
            c_u_limit -= 1
            print("bottom right")
        elif c[0] == r_u_limit and c[1] == c_l_limit:
            c[0] -= 1
            r_u_limit -= 1
            print("bottom left")
        elif c[0] == r_l_limit and c[1] == c_l_limit:
            c[1] += 1
            c_l_limit += 1
            print("top left")
        seen.append(c)
        start = c
        yield grid[c[0], c[1]]




def main():
    in_str = sys.stdin.readline()
    matches = re.search(r'^"(.+)"\s\((\d+),\s(\d+)\)\s(.+)', in_str)
    cipher, *cords, direction = matches.groups()
    direction = 1 if direction == 'clockwise' else -1
    cipher = [c.upper() for c in cipher if c.isalpha()]
    cords = [int(c) for c in cords]

    grid = np.full(cords[0] * cords[1], 'X')
    for x, c in enumerate(cipher):
        grid[x] = c
    grid = np.reshape(grid, cords)
    for x in get_next_letter(grid, cords, direction):
        print("letter is {}".format(x))

    cipher_text = [x for x in get_next_letter(grid, cords, direction)]
    print(cipher_text)


if __name__ == '__main__':
    main()