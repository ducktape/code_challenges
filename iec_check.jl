#!/usr/bin/env julia

    function check_rule(x::String)
        return (length(matchall(r"cei"i, x)) == length(matchall(r"ei"i, x))
                && !contains(x, "cie"))
    end

    open("./enable1.txt") do in_fh
        rule_breaks = Iterators.filter(x -> !check_rule(x), eachline(in_fh))
        println(length(collect(rule_breaks)))
    end